#ifndef ENUMERABLE_H
#define ENUMERABLE_H

#include <functional>

using namespace std;

template <class T>
class List;

template <class T>
class Enumerable
{
public:
	virtual void each(function<void(T)>) const = 0;
	bool all(function<bool(T)>) const;
	bool none(function<bool(T)>) const;
	bool any(function<bool(T)>) const;
	bool one(function<bool(T)>) const;
	int count() const;
	int count(T&) const;
	int count(function<bool(T)>) const;
	T& first() const;
	List<T>* first(int) const;
	T& reduce(function<T(T, T)>) const;
	T& reduce(const T&, function<T(T, T)>) const;
	List<T>* filter(function<bool(T)>) const;
};

template <class T>
bool Enumerable<T>::all(function<bool(T)> condition) const
{
	bool result = true;
	each([&condition, &result](T x) {
		if ( ! condition(x))
			result = false;
	});
	return result;
}

template <class T>
bool Enumerable<T>::none(function<bool(T)> condition) const
{
	bool result = true;
	each([&condition, &result](T x) {
		if (condition(x))
			result = false;
	});
	return result;
}

template <class T>
bool Enumerable<T>::any(function<bool(T)> condition) const
{
	bool result = false;
	each([&condition, &result](T x) {
		if (condition(x))
			result = true;
	});
	return result;
}

template <class T>
bool Enumerable<T>::one(function<bool(T)> condition) const
{
	int count = 0;
	each([&condition, &count](T x) {
		if (condition(x))
			count++;
	});
	return count == 1;
}

template <class T>
int Enumerable<T>::count() const
{
	int count = 0;
	each([&count](T x) {
		count++;
	});
	return count;
}

template <class T>
int Enumerable<T>::count(T& element) const
{
	int count = 0;
	each([&element, &count](T x) {
		if (x == element)
			count++;
	});
	return count;
}
	
template <class T>
int Enumerable<T>::count(function<bool(T)> condition) const
{
	int count = 0;
	each([&condition, &count](T x) {
		if (condition(x))
			count++;
	});
	return count;
}

template <class T>
T& Enumerable<T>::first() const
{
	T result;
	bool chosen = false;
	each([&result, &chosen](T x) {
		if ( ! chosen) {
			result = x;
			chosen = true;
		}
	});
	return result;
}

template <class T>
List<T>* Enumerable<T>::first(int count) const
{
	List<T>* list = new List<T>();
	each([&count, &list](T x) {
		if (count > 0) {
			list->add(x);
			count--;
	    }
	});
	return list;
}

template <class T>
T& Enumerable<T>::reduce(function<T(T, T)> reducer) const
{
	T sum;
	bool first_past = false;
	each([&reducer, &sum, &first_past](T x) {
		if (first_past) {
			sum = reducer(sum, x);
		} else {
			sum = x;
			first_past = true;
		}
	});
	return sum;
}

template <class T>
T& Enumerable<T>::reduce(const T& start, function<T(T, T)> reducer) const
{
	T sum = start;
	each([&reducer, &sum](T x) {
		sum = reducer(sum, x);
	});
	return sum;
}

template <class T>
List<T>* Enumerable<T>::filter(function<bool(T)> condition) const
{
	List<T>* list = new List<T>();
	each([&condition, &list](T x) {
		if (condition(x))
			list->add(x);
	});
	return list;
}

#endif //ENUMERABLE_H